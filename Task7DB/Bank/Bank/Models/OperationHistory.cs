﻿using System;

namespace Domain
{
    public class OperationHistory
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
        public Account Account { get; set; }
        public int AccountId { get; set; }
        public int OperationTypeId { get; set; }
    }
}
