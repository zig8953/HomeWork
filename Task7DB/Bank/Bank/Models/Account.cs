﻿using System;

namespace Domain
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime? OpeningDate { get; set; }
        public decimal CashAll { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
