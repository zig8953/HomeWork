﻿namespace Domain
{
    public class StatusUser
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
