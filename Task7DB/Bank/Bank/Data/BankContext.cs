﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;

namespace Infrastructure
{
    public class BankContext : DbContext
    {
        public DbSet<User> users { get; set; }
        public DbSet<Account> accounts { get; set; }
        public DbSet<OperationHistory> operationHistories { get; set; }
        public DbSet<OperationType> operationTypes { get; set; }
        public DbSet<StatusUser> statusUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<StatusUser>().HasData
                (
                    new StatusUser[]
                    {
                       new StatusUser() { Description = "Active", Id = 1 },
                       new StatusUser() { Description = "Remote", Id = 2 }
                    }
                );

            modelBuilder.Entity<OperationType>().HasData
                (
                    new OperationType[]
                    {
                       new OperationType() { Description = "Refill", Id = 1 },
                       new OperationType() { Description = "Take", Id = 2 }
                    }
                );


            modelBuilder.Entity<User>().HasData
               (
                   new User[]
                   {
                       new User() { Id = 1,
                           FirstName = "Ivan",
                           SurName = "Ivanov",
                           Password = "1qazxsw2",
                           Phone = "+7 705 751 92 93",
                           StatusUserId = 1,
                           RegistrationDate = DateTime.Now },
                   }
               );

            modelBuilder.Entity<Account>().HasData
              (
                  new Account[]
                  {
                       new Account() {Id = 1,
                           CashAll = 100,
                           OpeningDate = DateTime.Now,
                           UserId = 1 
                       },
                      
                  }
              );

        }

        public BankContext(DbContextOptions<BankContext> options) : base(options) 
        { 
            Database.EnsureCreated();
        }

    }
    
    public class EFDBContextFactory : IDesignTimeDbContextFactory<BankContext>
    {
        public BankContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BankContext>();
            return new BankContext(optionsBuilder.Options);
        }
    }

   
}
