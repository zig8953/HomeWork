﻿using Application;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Infrastructure
{
    public class OperationRepository : IRepository<OperationType>
    {
        private readonly BankContext _context;

        public OperationRepository(BankContext context)
        {
            _context = context;
        }

        public void Create(OperationType item)
        {
            _context.operationTypes.Add(item);
        }

        public void Delete(int id)
        {
            OperationType operationType = _context.operationTypes.Find(id);
            if (operationType != null)
                _context.operationTypes.Remove(operationType);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        private bool disposed = false;
        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public OperationType Get(int id)
        {
            return _context.operationTypes.Find(id);
        }

        public IEnumerable<OperationType> GetList()
        {
            return _context.operationTypes;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(OperationType item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
