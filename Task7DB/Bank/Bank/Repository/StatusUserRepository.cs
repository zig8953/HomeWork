﻿using Application;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;


namespace Infrastructure
{
    public class StatusUserRepository : IRepository<StatusUser>
    {
        readonly private BankContext _context;

        public StatusUserRepository(BankContext context)
        {
            _context = context;
        }
        public void Create(StatusUser item)
        {
            _context.statusUsers.Add(item);
        }

        public void Delete(int id)
        {
            StatusUser statusUser = _context.statusUsers.Find(id);
            if (statusUser != null)
                _context.statusUsers.Remove(statusUser);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;
        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public IEnumerable<StatusUser> GetList()
        {
            return _context.statusUsers;
        }

        public StatusUser Get(int id)
        {
            return _context.statusUsers.Find(id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(StatusUser item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
