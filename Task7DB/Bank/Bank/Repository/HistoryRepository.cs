﻿using Application;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Infrastructure
{
    public class HistoryRepository : IRepository<OperationHistory>
    {
        readonly private BankContext _context;

        public HistoryRepository(BankContext context)
        {
            _context = context;
        }

        public void Create(OperationHistory item)
        {
            _context.operationHistories.Add(item);
        }

        public void Delete(int id)
        {
            OperationHistory operationHistory = _context.operationHistories.Find(id);
            if (operationHistory != null)
                _context.operationHistories.Remove(operationHistory);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;
        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public OperationHistory Get(int id)
        {
            return _context.operationHistories.Find(id);
        }

        public IEnumerable<OperationHistory> GetList()
        {
            return _context.operationHistories;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(OperationHistory item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
