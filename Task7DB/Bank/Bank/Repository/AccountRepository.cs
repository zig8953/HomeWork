﻿using Application;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Infrastructure
{
    public class AccountRepository : IRepository<Account>
    {
        private readonly BankContext _context;

        public AccountRepository(BankContext context)
        {
            _context = context;
        }

        public void Create(Domain.Account item)
        {
            _context.accounts.Add(item);
        }

        public void Delete(int id)
        {
            Domain.Account account = _context.accounts.Find(id);
            if (account != null)
                _context.accounts.Remove(account);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;
        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public Domain.Account Get(int id)
        {
            return _context.accounts.Find(id);
        }

        public IEnumerable<Domain.Account> GetList()
        {
            return _context.accounts;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Account item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
