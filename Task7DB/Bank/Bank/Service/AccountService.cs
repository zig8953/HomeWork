﻿using Application;
using Domain;
using System.Collections.Generic;

namespace Infrastructure
{
    public class AccountService
    {
        readonly IRepository<Account> _context;

        public AccountService(Infrastructure.BankContext context)
        {
            _context = new AccountRepository(context);
        }

        public IEnumerable<Account> GetAccount()
        {
            return _context.GetList();
        }

        public void Add(Account status)
        {
            _context.Create(status);
        }

        public void Update(Account status)
        {
            _context.Update(status);
        }

        public void Save()
        {
            _context.Save();
        }

        public Account Get(int id)
        {
            return _context.Get(id);
        }
        public void Delete(int id)
        {
            _context.Delete(id);
        }
    }
}
