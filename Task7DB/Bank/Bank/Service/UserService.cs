﻿using Application;
using Domain;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System.Collections.Generic;


namespace Infrastructure
{
    public class UserService
    {
        readonly IRepository<User> _context;

        public UserService(Infrastructure.BankContext context, IDatabase redis, ISubscriber subscriber, ILogger<User> logger)
        {
            _context = new UsersRepository(context, redis, subscriber, logger);
        }

        public IEnumerable<User> GetUser()
        {
            return _context.GetList();
        }

        public void Add(User user)
        {
            _context.Create(user);
        }

        public void Update(User user)
        {
            _context.Update(user);
        }

        public void Save()
        {
            _context.Save();
        }

        public User Get(int id)
        {
            return _context.Get(id);
        }
        public void Delete(int id)
        {
            _context.Delete(id);
        }
    }
}
