﻿using Application;
using Domain;
using System.Collections.Generic;

namespace Infrastructure
{
    public class StatusUserService
    {
        readonly IRepository<StatusUser> _context;

        public StatusUserService(Infrastructure.BankContext context)
        {
            _context = new StatusUserRepository(context);
        }

        public IEnumerable<StatusUser> GetStatusUser()
        {
            return _context.GetList();
        }

        public void Add(StatusUser status)
        {
            _context.Create(status);
        }

        public void Update(StatusUser status)
        {
            _context.Update(status);
        }

        public void Save()
        {
            _context.Save();
        }

        public StatusUser Get(int id)
        {
            return _context.Get(id);
        }

        public void Delete(int id)
        {
            _context.Delete(id);
        }
    }
}
