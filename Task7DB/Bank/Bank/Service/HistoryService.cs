﻿using Application;
using Domain;
using System.Collections.Generic;

namespace Infrastructure
{
    public class HistoryService
    {
        readonly IRepository<OperationHistory> _context;

        public HistoryService(Infrastructure.BankContext context)
        {
            _context = new HistoryRepository(context);
        }

        public IEnumerable<OperationHistory> GetOperationHistory()
        {
            return _context.GetList();
        }

        public void Add(OperationHistory status)
        {
            _context.Create(status);
        }

        public void Update(OperationHistory status)
        {
            _context.Update(status);
        }

        public void Save()
        {
            _context.Save();
        }

        public OperationHistory Get(int id)
        {
            return _context.Get(id);
        }

        public void Delete(int id)
        {
            _context.Delete(id);
        }
    }
}
