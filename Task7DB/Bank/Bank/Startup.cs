using Application;
using Bank.Data;
using Domain;
using Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StackExchange.Redis;

namespace Bank
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();

            services.AddScoped<Infrastructure.UserService>();
            services.AddScoped<Infrastructure.StatusUserService>();
            services.AddScoped<Infrastructure.OperationService>();
            services.AddScoped<Infrastructure.AccountService>();
            services.AddScoped<Infrastructure.HistoryService>();

            services.AddDbContext<BankContext>(options =>
                options.UseSqlite("Filename=Bank.db"));

            services.AddScoped<IRepository<User>, UsersRepository>();
            services.AddScoped<IRepository<StatusUser>, StatusUserRepository>();
            services.AddScoped<IRepository<OperationType>, OperationRepository>();
            services.AddScoped<IRepository<Account>, AccountRepository>();
            services.AddScoped<IRepository<OperationHistory>, HistoryRepository>();

            try
            {
               
                    services.AddSingleton<IConnectionMultiplexer>(_ => ConnectionMultiplexer.Connect("localhost:32769, abortConnect = false"));
                    services.AddTransient(sp => sp.GetRequiredService<IConnectionMultiplexer>().GetDatabase());
                    services.AddTransient(sp => sp.GetRequiredService<IConnectionMultiplexer>().GetSubscriber());
                
                
            }
            catch (RedisConnectionException e2) { }
       
     





}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
