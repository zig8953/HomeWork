﻿using System;
using System.Collections.Generic;

namespace Application
{
    public interface IRepository<T> : IDisposable
        where T : class
    {
        IEnumerable<T> GetList(); //getting all objects
        T Get(int id); //getting one object by id
        void Create(T item); //object creation
        void Update(T item); //object update
        void Delete(int id); //delete an object by id
        void Save();  //saving changes
    }
}
