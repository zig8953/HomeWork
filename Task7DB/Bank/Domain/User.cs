﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string SurName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string Password { get; set; }
        //[Display(Name = "Description")]
        public StatusUser StatusUser { get; set; }
        public int StatusUserId { get; set; }
    }
}
