﻿using Application;
using Domain;
using System.Collections.Generic;

namespace Infrastructure
{
    public class OperationService
    {
        readonly IRepository<OperationType> _context;

        public OperationService(Infrastructure.BankContext context)
        {
            _context = new OperationRepository(context);
        }

        public IEnumerable<OperationType> GetOperationType()
        {
            return _context.GetList();
        }

        public void Add(OperationType status)
        {
            _context.Create(status);
        }

        public void Update(OperationType status)
        {
            _context.Update(status);
        }

        public void Save()
        {
            _context.Save();
        }

        public OperationType Get(int id)
        {
            return _context.Get(id);
        }

        public void Delete(int id)
        {
            _context.Delete(id);
        }
    }
}
