﻿using Application;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class UsersRepository : IRepository<User>
    {
        private readonly BankContext _context;
        private readonly ILogger _logger;
        private readonly IDatabase _redis;
        private readonly ISubscriber _subscriber;
        private readonly byte RedisID = 1;
        public UsersRepository(BankContext context, IDatabase redis, ISubscriber subscriber, ILogger<User> logger)
        {
            _context = context;
            _redis = redis;
            _subscriber = subscriber;
            _logger = logger;
        }

        public void Create(User item)
        {
            _context.users.Add(item);
        }

        public void Delete(int id)
        {
            User user = _context.users.Find(id);
            if (user != null)
                _context.users.Remove(user);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public User Get(int id)
        {
            return _context.users.Find(id);
        }

        public IEnumerable<User> GetList()
        {
            var fromCache = GetUserFromCache(RedisID);
            if (fromCache.Result != null)
            {
                return fromCache.Result.ToList();
            }

            _ = AddUserToCache(_context.users.ToList());

            return _context.users.ToList();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(User item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        private async Task<IEnumerable<User>> GetUserFromCache(int id)
        {
            var redisKey = GetCacheKey(id);
            string cache = await _redis.StringGetAsync(redisKey);

            if (!string.IsNullOrEmpty(cache))
            {
                try
                {
                    return JsonSerializer.Deserialize<IEnumerable<User>>(cache);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Could not deserialize user");
                }
            }

            return null;
        }

        private async Task AddUserToCache(IEnumerable<User> user)
        {
            if (user != null)
            {
                var redisKey = GetCacheKey(RedisID);
                await _redis.StringSetAsync(redisKey, JsonSerializer.Serialize(user), flags: CommandFlags.FireAndForget);
            }
        }

        private static string GetCacheKey(int id)
        {
            return $"users:id_{id}";
        }
    }
}
