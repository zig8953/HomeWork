﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructure
{
    public class BankContext : DbContext
    {
        public DbSet<User> users { get; set; }
        public DbSet<Account> accounts { get; set; }
        public DbSet<OperationHistory> operationHistories { get; set; }
        public DbSet<OperationType> operationTypes { get; set; }
        public DbSet<StatusUser> statusUsers { get; set; }

        public BankContext(DbContextOptions<BankContext> options) : base(options) { Database.EnsureCreated(); }

    }


    public class EFDBContextFactory : IDesignTimeDbContextFactory<BankContext>
    {
        public BankContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BankContext>();
            return new BankContext(optionsBuilder.Options);
        }
    }
}
