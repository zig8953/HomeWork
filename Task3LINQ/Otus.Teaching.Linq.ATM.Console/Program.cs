﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        
        static void Main(string[] args)
        {
            var atmManager = CreateATMManager();
            Heder();

            while (true)
            {
                System.Console.WriteLine("Введите логин:");
                string login = System.Console.ReadLine();
                System.Console.WriteLine("Введите пароль:");
                string password = System.Console.ReadLine();

                if (CheckValue(login) || CheckValue(password))
                {
                    ColorText("Неверный ввод логина или пароля", ConsoleColor.Red);
                    continue;
                }
                    
                var user = atmManager.Login(login, password).ToList();

                System.Console.WriteLine();
                if (user.Count > 0)
                    System.Console.WriteLine($"Добро пожаловать! {user[0].FirstName} {user[0].SurName}");
                else
                {
                    ColorText("Неверный логин или пароль.", ConsoleColor.Red);
                    continue;
                }

                while (true)
                {
                    System.Console.WriteLine();
                    switch (Operation())
                    {
                        case 1:
                            {
                                System.Console.WriteLine("Cчета:");
                                foreach (var item in atmManager.AccountsAll(user))
                                {
                                    System.Console.WriteLine($"Номер счета: {item.Id} Баланс: {item.CashAll}");
                                }
                            }
                            continue;
                        case 2:
                            {
                                foreach (var account in atmManager.HistoryAll(user))
                                {
                                    System.Console.WriteLine($"Номер счета: {account.Account}");
                                    System.Console.WriteLine("Операции:");

                                    foreach (var history in account.History)
                                    {
                                        System.Console.WriteLine(string.Format("{0} Дата {1} Сумма {2,12:0#}", 
                                            (history.OperationType == Core.Entities.OperationType.InputCash ? "Пополнение" : "Снятие"), 
                                            history.OperationDate, history.CashSum));
                                    }
                                }
                            }
                            continue;

                        case 3:
                            {
                                foreach (var operationInput in atmManager.AllOperationInput())
                                {
                                    System.Console.WriteLine($"Пользователь: {operationInput.SurName}");
                                    foreach (var history in operationInput.History)
                                    {
                                        System.Console.WriteLine($"Дата: {history.OperationDate} Пополнение: {history.CashSum}");
                                    }
                                }

                            }
                            continue;

                        case 4:
                            {
                                System.Console.WriteLine("Укажите суму:");
                                decimal amount = Convert.ToDecimal(System.Console.ReadLine());

                                System.Console.WriteLine($"Пользователи у которых сумма на счету больше {amount}.");
                                foreach (var item in atmManager.AmountLarge(amount))
                                {
                                    System.Console.WriteLine($"{item.SurName} ");
                                }

                            }
                            continue;
                        case 5:
                            {
                                break;         
                            }

                        default:
                            {
                                ColorText("Выберите из списка операцию.", ConsoleColor.Red);
                                continue;
                            }
                    }
                    break;
                }
            }
           
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

        public static void Heder() 
        {
            System.Console.ForegroundColor = ConsoleColor.DarkGreen;
            System.Console.WriteLine("AO Timkiv Bank\u263A");
            System.Console.WriteLine(new string('-', 30));
            System.Console.ForegroundColor = ConsoleColor.White;
        }

        public static byte Operation() 
        {
            ColorText("1 Вывод данных о всех счетах заданного пользователя;", ConsoleColor.DarkGray);
            ColorText("2 Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту", ConsoleColor.DarkGray);
            ColorText("3 Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта", ConsoleColor.DarkGray);
            ColorText("4 Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой", ConsoleColor.DarkGray);
            ColorText("5 Выход", ConsoleColor.DarkGray);

            try
            {
                return Convert.ToByte(System.Console.ReadLine());
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static bool CheckValue(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return true;

            return false;
                
        }

        public static void ColorText(string text, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
            System.Console.WriteLine(text);
            System.Console.ForegroundColor = ConsoleColor.White;
        }

    }
}