﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class HistoryAllType
    {
        public int Account { get; set; }
        public List<OperationsHistory> History { get; set;}
    }

    public class AllOperationInputType 
    {
        public string SurName { get; set; }
        public List<OperationsHistory> History { get; set; }
    }

    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //Вывод информации о заданном аккаунте по логину и паролю;
        public IEnumerable<User> Login(string login, string Password)
        {
            if(string.IsNullOrEmpty(login) || string.IsNullOrEmpty(Password))
                throw new ArgumentException("Не указан логин или пароль.");

            return from u in Users 
                   where u.Login == login 
                   where u.Password == Password 
                   select u;
        }

        //Вывод данных о всех счетах заданного пользователя;
        public IEnumerable<Account> AccountsAll(IEnumerable<User> user)
        {
            var accounts = from u in user
                           join account in Accounts on u.Id equals account.UserId
                           select account;

            return accounts;
        }

        //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        public List<HistoryAllType> HistoryAll(IEnumerable<User> user)
        {

            var account = from a in Accounts
                where a.UserId == user.ToList()[0].Id
                select a;

            var result = account.GroupJoin(
                History,
                a => a.Id,
                h => h.AccountId,
                (a, h) => new
                {
                    id = a.Id,
                    History = h
                }
                ); 


            List<HistoryAllType> historyAllTypes = new List<HistoryAllType>();

            foreach (var accounts in result)
            {
                historyAllTypes.Add(new HistoryAllType() { Account = accounts.id, History = accounts.History.ToList() });
            }

            return historyAllTypes;
        }

        //Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;
        public List<AllOperationInputType> AllOperationInput() 
        {

            var user = (from u in Users
                       join a in Accounts on u.Id equals a.UserId
                       select new { u.SurName, id = a.Id }).Distinct();

            var account = user.GroupJoin(
                History,
                u => u.id,
                h => h.AccountId,
                (u, h) => new
                {
                    u.SurName,
                    History = h.Where(o => o.OperationType == OperationType.InputCash) 
                }).Where(a => a.History.Count() > 0);

            List<AllOperationInputType> allOperationInputType = new List<AllOperationInputType>();
            List<OperationsHistory> historyTypes = new List<OperationsHistory>();

            foreach (var itemAccount in account)
            {

                historyTypes.Clear();
                foreach (var history in itemAccount.History)
                {
                    historyTypes.Add(new OperationsHistory()
                    {
                        Id = history.Id,
                        AccountId = history.AccountId,
                        CashSum = history.CashSum,
                        OperationDate = history.OperationDate,
                        OperationType = history.OperationType
                    });
                }
                allOperationInputType.Add(new AllOperationInputType() { SurName = itemAccount.SurName, History = new List<OperationsHistory>(historyTypes)});


            }
            return (allOperationInputType);
        }
       
        //Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
        public List<User> AmountLarge(decimal cash)
        {
            return (from a in Accounts
                    join u in Users on a.UserId equals u.Id
                    where (a.CashAll > cash)
                    select u).Distinct().ToList();

           
        }

    }
}