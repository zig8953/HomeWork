﻿using System;
/// <summary>
/// The main Matrix class
/// Contains methods for working with matrices:
///     -Matrix Fill
///     -Matrix display
///     -Sum
///     -Difference
///     -Access to matrix elements
///     
/// Основной класс Matrix
/// Содержит методы для работы с матрицами:
///     -Заполнение
///     -Отображение матрицы
///     -Сумма
///     -Различие
///     -Доступ к матричным элементам    
/// 
/// </summary>
namespace Matrix
{
    class Matrix
    {
        readonly int[][] matrix;

        public Matrix(int row, int col)
        {
            if(row <= 0 || col <= 0)
                throw new ArgumentException("Не возможно создать матрицу.");

            matrix = new int[Math.Abs(row)][];
            for (int i = 0; i < Math.Abs(row); i++)
                matrix[i] = new int[Math.Abs(col)];
        }

        //Fill the matrix with random numbers from 10 to 90
        //Заполнить матрицу случайными числами от 10 до 90
        public void Fill()
        {
            Random rand = new Random();
            for (int i = 0; i < matrix.Length; i++)
                for (int j = 0; j < matrix[i].Length; j++)
                    matrix[i][j] = rand.Next(10, 90);
        }

        //Overloaded method for reflect matrix
        //Перегруженный метод для отражает матрицы
        public override string ToString()
        {
            string ToString(int startRow, int startCol, int endRow, int endCol)
            {
                if (startRow < 0 || startCol < 0 || endRow > matrix.Length || endCol > matrix[0].Length)
                {
                    return "Попытка обращения за пределы массива.";
                }

                if (startRow > endRow || startCol > endCol)
                {
                    return "Неверно заданы координаты конечной точки";
                }

                string resultValue = default;

                for (int i = startRow; i < endRow; i++)
                {
                    for (int j = startCol; j < endCol; j++)
                        resultValue += string.Format("{0}  ", matrix[i][j]);
                    resultValue += "\n";
                }

                return resultValue;
            }
            return ToString(0, 0, matrix.Length, matrix[0].Length);
        }

        //Overloaded operator returns the sum of two matrices
        //Перегруженный оператор возвращает сумму двух матриц 
        public static Matrix operator + (Matrix matrixFirs, Matrix matrixSecond)
        {
            if(matrixFirs.matrix.Length != matrixSecond.matrix.Length || 
               matrixFirs.matrix[0].Length != matrixSecond.matrix[0].Length)
                throw new ArgumentException("Сложение матриц невозможно.");

            Matrix newMatrix = new Matrix(matrixFirs.matrix.Length, matrixFirs.matrix[0].Length);

            for (int i = 0; i < Math.Abs(matrixFirs.matrix.Length); i++)
                newMatrix.matrix[i] = new int[Math.Abs(matrixFirs.matrix[0].Length)];

            for (int i = 0; i < matrixFirs.matrix.Length; i++)
                for (int j = 0; j < matrixFirs.matrix[i].Length; j++)
                    newMatrix.matrix[i][j] = matrixFirs.matrix[i][j] + matrixSecond.matrix[i][j];

            return newMatrix;
        }

        //Overloaded operator returns the difference of two matrices
        //Перегруженный оператор возвращает разницу двух матриц 
        public static Matrix operator - (Matrix matrixFirs, Matrix matrixSecond)
        {
            if (matrixFirs.matrix.Length != matrixSecond.matrix.Length ||
              matrixFirs.matrix[0].Length != matrixSecond.matrix[0].Length)
                throw new ArgumentException("Вычитание матриц невозможно.");

            Matrix newMatrix = new Matrix(matrixFirs.matrix.Length, matrixFirs.matrix[0].Length);

            for (int i = 0; i < Math.Abs(matrixFirs.matrix.Length); i++)
                newMatrix.matrix[i] = new int[Math.Abs(matrixFirs.matrix[0].Length)];

            for (int i = 0; i < matrixFirs.matrix.Length; i++)
                for (int j = 0; j < matrixFirs.matrix[i].Length; j++)
                    newMatrix.matrix[i][j] = matrixFirs.matrix[i][j] - matrixSecond.matrix[i][j];

            return newMatrix;
        }

        //Indexer for accessing matrix elements
        //Индексатор для доступа к элементам матрицы
        public int this[int row, int col]
        {
            get
            {
                return matrix[row][col];
            }
            set
            {
                 matrix[row][col] = value;
            }
        }
    }
}
