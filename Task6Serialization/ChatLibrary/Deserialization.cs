﻿using System.Text.Json;

namespace ChatLibrary
{
    public class Deserialization
    {
        private readonly string _message;

        public Deserialization(string message)
        {
            if (string.IsNullOrEmpty(message))
                ErrorMessage.Exception("Ведено пустое сообщение", 500);
            else
                this._message = message;
        }

        public string Json()
        {            
            Message result = JsonSerializer.Deserialize<Message>(_message);
            return $"{result.Text} : {result.DateTime.ToString("MM/dd/yyyy H:mm")}";
        }
               
    }
}
