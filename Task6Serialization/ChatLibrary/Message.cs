﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatLibrary
{
    public class Message
    {
        [Required(ErrorMessage = "Сообщение не установлено")]
        public string Text { get; set; }
        public DateTime DateTime { get; set; }

    }
}