﻿using System;

namespace ChatLibrary
{
    public static class ErrorMessage
    {

        public static void Exception(string textError, int statusCode)
        { 
            Console.ForegroundColor = ConsoleColor.Red; 
            Console.WriteLine($"Ошибка: {textError} Код: {statusCode}");
            Console.ResetColor();
        }
    

       
   }
}
