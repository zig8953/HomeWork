﻿using System;
using System.Text;
using System.Text.Json;

namespace ChatLibrary
{
    public class Serialization
    {
        private readonly Message _message;

        public Serialization(Message message)
        {
            if (String.IsNullOrEmpty(message.Text))
                ErrorMessage.Exception("Введено пустое сообщение", 500);
            else
                this._message = message;
        }

        public byte[] Json() 
        {
            return Encoding.Unicode.GetBytes(JsonSerializer.Serialize(_message));
        } 
    }
}
