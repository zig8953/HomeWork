﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using ChatLibrary; 

namespace Client
{
    public static class Program
    {
        private static string _userName;
        private static string Host { get => "127.0.0.1"; }
        private static int Port { get => new Settings().Load().Port; }
        private static TcpClient _client;
        private static NetworkStream _stream;

        

        static void Main()
        {
            Console.Write("Введите свое имя: ");
            _userName = Console.ReadLine();
            _client = new TcpClient();
            try
            {
                _client.Connect(Host, Port); //client connection
                _stream = _client.GetStream(); //get a stream

                string message = _userName;
                byte[] data = Encoding.Unicode.GetBytes(message);
                _stream.Write(data, 0, data.Length);

                //start a new thread to receive data
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //start of thread
                Console.WriteLine("Добро пожаловать, {0}", _userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        //sending messages
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");
                     
            while (true)
            {
                Message message = new Message
                {
                    DateTime = DateTime.Now,
                    Text = Console.ReadLine()
                };
                byte[] data = new Serialization(message).Json();

                _stream.Write(data, 0, data.Length);
            }

        }
        //get messages
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; //buffer for get data
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = _stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (_stream.DataAvailable);

                    string message = builder.ToString();
                    Console.WriteLine(message);//message output
                }
                catch(Exception)
                {
                    Console.WriteLine("Подключение прервано!" ); //connection was interrupted
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
                _stream?.Close();//stream shutdown
                _client?.Close();//client disconnection
            Environment.Exit(0); //process completion
        }
    }
}
