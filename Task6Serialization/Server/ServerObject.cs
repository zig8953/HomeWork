﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using ChatLibrary;

namespace Server
{
    class ServerObject
    {
        private static TcpListener _tcpListener; //server to listen
        private readonly List<ClientObject> _clients = new List<ClientObject>(); //all connections

        protected internal void AddConnection(ClientObject clientObject)
        {
            _clients.Add(clientObject);
        }
        protected internal void RemoveConnection(string id)
        {
            //get id closed connection
            ClientObject client = _clients.FirstOrDefault(c => c.Id == id);
            //and remove it from the connection list
            if (client != null)
                _clients.Remove(client);
        }
        //listening for incoming connections
        protected internal void Listen()
        {
            try
            {
                _tcpListener = new TcpListener(IPAddress.Any, new Settings().Load().Port);
                _tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = _tcpListener.AcceptTcpClient();

                    ClientObject clientObject = new ClientObject(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        //broadcast message to connected clients
        protected internal void BroadcastMessage(string message, string id)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            for (int i = 0; i < _clients.Count; i++)
            {
                if (_clients[i].Id != id) //if the id of the client is not equal to the id of the sender
                {
                    _clients[i].Stream.Write(data, 0, data.Length); //data transfer
                }
            }
        }
        // отключение всех клиентов
        protected internal void Disconnect()
        {
            _tcpListener.Stop(); //server shutdown

            for (int i = 0; i < _clients.Count; i++)
            {
                _clients[i].Close(); //client disconnection
            }
            Environment.Exit(0); //process completion
        }
        
    }
}
