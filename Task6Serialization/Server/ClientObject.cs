﻿using System;
using System.Net.Sockets;
using System.Text;
using ChatLibrary;

namespace Server
{
    class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        private string _userName;
        private readonly TcpClient _client;
        private readonly ServerObject _server; //server object

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            _client = tcpClient;
            _server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = _client.GetStream();
                //get username
                string message = GetMessage();
                _userName = message;

                message = _userName + " вошел в чат";
                //send a chat entry message to all connected users
                _server.BroadcastMessage(message, this.Id);
                Console.WriteLine(message);
                //in an infinite loop to receive messages from the client
                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        message = String.Format("{0}: {1}", _userName, new Deserialization(message).Json());
                        Console.WriteLine(message);
                        _server.BroadcastMessage(message, this.Id);
                    }
                    catch
                    {
                        message = String.Format("{0}: покинул чат", _userName);
                        Console.WriteLine(message);
                        _server.BroadcastMessage(message, this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                //in case of exit from the cycle, close the resources
                _server.RemoveConnection(this.Id);
                Close();
            }
        }

        //reading an incoming message and converting to a string
        private string GetMessage()
        {
            byte[] data = new byte[64]; //buffer for received data
            StringBuilder builder = new StringBuilder();
            do
            {
                int bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);

            return builder.ToString();
        }

        //closing connection
        protected internal void Close()
        {
                Stream?.Close();
                _client?.Close();
        }
    }
}
