﻿using System;
using System.Threading;

namespace Server
{
    class Program
    {
        private static ServerObject _server; //server
        private static Thread _listenThread; //stream to listen
        static void Main()
        {
            try
            {
                _server = new ServerObject();
                _listenThread = new Thread(new ThreadStart(_server.Listen));
                _listenThread.Start(); //start of flow
            }
            catch (Exception ex)
            {
                _server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }
    }
}
