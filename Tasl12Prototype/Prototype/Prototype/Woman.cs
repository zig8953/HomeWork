﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Woman : Human, IMyCloneable<Woman>
    {
        public Woman()
        {
            Class = "Woman";
            State += "Woman signs ";
        }

        Woman IMyCloneable<Woman>.Clone()
        {
            return new Woman();
        }
    }
}
