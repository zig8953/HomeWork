﻿using System;

namespace Prototype
{
    class Prototype : IMyCloneable<Prototype>
    {
        public string Class { get; set; }
        public string State { get; set; }

        public Prototype()
        {
            Class = "Biological system ";
            State = "";
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        Prototype IMyCloneable<Prototype>.Clone()
        {
            return new Prototype();
        }

        public void GetInfo()
        {
            Console.WriteLine($"Class: { Class} State: {State}");
        }
    }
}
