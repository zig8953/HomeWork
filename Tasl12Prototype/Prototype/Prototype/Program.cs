﻿using System;


// Преимущество клонирования с использованием MemberwiseClone() в том, что
// при клонировании не вызывается конструктор, а клонирование происходит через
// копирование дампа памяти - тела оригинала.


namespace Prototype
{
    class Program
    {
        static void Main()
        {
            IMyCloneable<Man> original = new Man();
            original.GetInfo();

            Man clone = original.Clone();

            clone.GetInfo();

            Human changeClone = clone;
            changeClone.Class = "Alien";

            changeClone.GetInfo();
            original.GetInfo();

            Console.ReadKey();
        }
    }


   
}
