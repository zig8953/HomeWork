﻿
namespace Prototype
{

    interface IMyCloneable<T>
    {
       T Clone();
       void GetInfo();
    }
}
