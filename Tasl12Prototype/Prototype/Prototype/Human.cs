﻿
namespace Prototype
{
    class Human : Prototype, IMyCloneable<Human>
    {
        public Human()
        {
            Class = "Human";
            State += "Common signs of a human ";
        }

        Human IMyCloneable<Human>.Clone()
        {
            return new Human();
        }
    }
}
