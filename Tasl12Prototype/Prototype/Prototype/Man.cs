﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    class Man : Human, IMyCloneable<Man>
    {
        public Man() : base()
        {
            Class = "Man";
            State += "Male signs ";
        }

        Man IMyCloneable<Man>.Clone()
        {
            return new Man();
        }
    }
}
