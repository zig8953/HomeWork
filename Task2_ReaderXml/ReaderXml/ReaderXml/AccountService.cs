﻿using System;
using System.Collections.Generic;
namespace ReaderXml
{
    class AccountService : IAccountService
    {
        Repository Repository;

        public AccountService(Repository repository)
        {
            Repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (string.IsNullOrWhiteSpace(account.FirstName) || string.IsNullOrWhiteSpace(account.LastName))
                throw new ArgumentException("Параметры заполнены не верно.");

            if (GetAge(account.BirthDate) <= 18)
                throw new ArgumentException("Возраст должен быть больше 18 лет.");

            Repository.Add(account);
        }

        private static int GetAge(DateTime birthDate)
        {
            var now = DateTime.Today;
            return now.Year - birthDate.Year - 1 +
                ((now.Month > birthDate.Month || now.Month == birthDate.Month && now.Day >= birthDate.Day) ? 1 : 0);
        }

    }
}
