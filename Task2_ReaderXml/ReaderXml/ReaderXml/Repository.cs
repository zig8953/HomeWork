﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ReaderXml
{
    public class Repository : IRepository<Account>
    {
        List<Account> accounts = new List<Account>();
        XmlSerializer xml = new XmlSerializer(typeof(List<Account>));
        string path = "User.xml";

        public void Add(Account item)
        {
            accounts.Add(item);

            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
                xml.Serialize(file, accounts);
            }
        }

        public IEnumerable<Account> GetAll()
        {
            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
                var nextLine = xml.Deserialize(file) as List<Account>;

                for (int i = 0; i < nextLine.Count; i++)
                {
                    yield return nextLine[i];
                }
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            using (var file = new FileStream(path, FileMode.OpenOrCreate))
            {
                var user = xml.Deserialize(file) as List<Account>;

                for (int i = 0; i < user.Count; i++)
                {
                    if (predicate(user[i]))
                    {
                        return user[i];
                    }
                }

                return new Account { FirstName = "", LastName = "", BirthDate = new DateTime(1994, 01, 01) };
            }
        }
    }
}
