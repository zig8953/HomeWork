﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReaderXml
{
    interface IAlgorithm
    {
        public int Search (List<Component> document, string value);
      
    }
}
