﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReaderXml
{
    [Serializable]
    class Group
    {
        public string Name { get; }

        public Group(string name)
        {
            Name = name;
        }

    }
}
