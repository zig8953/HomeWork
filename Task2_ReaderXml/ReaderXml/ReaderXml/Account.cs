﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReaderXml
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
