﻿using System;
using System.Xml.Linq;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Collections;
using System.Linq;
using Moq;

namespace ReaderXml
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;

            var groups = new List<Group>();

            groups.Add(new Group("Panel1"));
            groups.Add(new Group("Panel2"));

            var componet = new List<Component>();

            componet.Add(new Component("Edit", "AppealID", "123") { Group = groups[0] });
            componet.Add(new Component("DateTimePicker", "AppealDate", "18/03/2020") { Group = groups[0] });
            componet.Add(new Component("DateTimePicker", "AppealDate", "18/03/2020") { Group = groups[0] });

            var json = new DataContractJsonSerializer(typeof(List<Component>));

            using (var file = new FileStream("Document.json", FileMode.OpenOrCreate))
            {
                json.WriteObject(file, componet);
            }

            Deserializer week = new Deserializer(@"Document.json");

            string str = default;

            foreach (var component in week)
            {
                Console.WriteLine(component);
                str = component;
            }

            Console.WriteLine($" Количество записей: {week.Search(componet, str)}");

            Repository repository = new Repository();
            AccountService accountService = new AccountService(repository);
            

            List<Account> accounts = new List<Account>();
            accounts.Add(new Account() { FirstName = "Oleg", LastName = "Timkiv", BirthDate = new DateTime(1994, 09, 27)});


            Account account = new Account() { FirstName = "Oleg", LastName = "Timkiv", BirthDate = new DateTime(1994, 09, 27) };
            Account account1 = new Account() { FirstName = "Ivan", LastName = "Ivanov", BirthDate = new DateTime(1991, 1, 01) };

            accountService.AddAccount(account);
            accountService.AddAccount(account1);

    


            Console.WriteLine();

            Console.WriteLine(" Все записей:");
            foreach (var item in repository.GetAll().ToList())
            {
                Console.WriteLine($" {item.FirstName}"); ;
            }

            Console.WriteLine($" Проверка {repository.GetOne(account => account.LastName == "Timkiv").FirstName}");

            Mock<IRepository<Account>> mock = new Mock<IRepository<Account>>();

            mock.Setup(r => r.GetAll()).Returns(accounts);

            var Test = mock.Object.GetAll();

            mock.Verify(foo => foo.GetAll(), Times.AtLeastOnce());

            Console.ReadKey();
        }
    }
    //Реализация IEnumerable<T>
    //Чтения json    
    class Deserializer : IEnumerable<string>, IAlgorithm
    {
        private string _filePath;

        public Deserializer(string filePath)
        {
            _filePath = filePath;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return new StreamReaderEnumerator(_filePath);
        }
        //Подсчет количество записей 
        public int Search(List<Component> document, string value)
        {
           IEnumerable<String> stringsFound;
            try
            {
                stringsFound =
                      from line in new Deserializer(@"Document.json")
                      where line.Contains(value)
                      select line;
                return stringsFound.Count();
            }
            catch (FileNotFoundException)
            { 
                throw new FileNotFoundException("Не возможно создать матрицу.");
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

    }
    public class StreamReaderEnumerator : IEnumerator<string>
    {
        private List<Component> document;
        int position = -1;

        public StreamReaderEnumerator(string filePath)
        {
            var json = new DataContractJsonSerializer(typeof(List<Component>));
            using (var file = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                document = json.ReadObject(file) as List<Component>;
            }
        }

        public string Current
        {
            get
            {
                if (position == -1 || position >= document.Count)
                    throw new InvalidOperationException();
                return $" Group: {document[position].Group.Name}, {document[position].Name}, {document[position].Value}, {document[position].Value};"  ;
            }
        }

        object IEnumerator.Current => throw new NotImplementedException();

        public void Dispose()
        {
            position = -1;
            document.Clear();
        }

        public bool MoveNext()
        {
            if (position < document.Count - 1)
            {
                position++;
                return true;
            }
            else
                return false;

        }

        public void Reset()
        {
            position = -1;
        }

    }
}
