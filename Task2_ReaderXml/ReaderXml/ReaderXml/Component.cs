﻿using System;
using System.Runtime.Serialization;

namespace ReaderXml
{
    [DataContract]
    class Component 
    {
        [DataMember]
        public string Type { get; set; }
        
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public Group Group { get; set; }

        public Component(string type, string name, string value)
        {
            Type = type;
            Name = name;
            Value = value;
        }
    }
}
