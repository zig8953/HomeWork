﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Reflection;
using System.Linq;

namespace Reflection
{
    public static class Serializator
    {

        public static bool FlagMethod {get; set;}

        public static string ToCsv(this object o)
        {
            var sb = new StringBuilder();
            var values = new List<string>();
            object data = default;
            
            values.Clear();
          
            Type type = typeof(Data);

            MethodInfo[] methodInfos = type.GetMethods();

            FieldInfo[] fieldInfo = type.GetFields(BindingFlags.NonPublic
            | BindingFlags.Public
            | BindingFlags.Instance);

            for (int i = 0; i < fieldInfo.Length; i++)
            {
                values.Add(string.Format("\"{0}\"", fieldInfo[i].Name));
            }
               
            sb.AppendLine(string.Join(";", values.ToArray()));
            values.Clear();

            if (FlagMethod)
                data = methodInfos[0].Invoke(o, null);

            foreach (var item in fieldInfo)
            {
                if (FlagMethod)
                    values.Add(string.Format("\"{0}\"", item.GetValue(data)));
                else
                    values.Add(string.Format("\"{0}\"", "0"));
            }

            return sb.AppendLine(string.Join(";", values.ToArray())).ToString() ;
        }

        public static T FromCsv<T>(this string s) where T : new()
        {
            string[] columns;
            string[] rows;

            T t = new T();

            using (var stream = new FileStream(s, FileMode.Open, FileAccess.Read))
            {
                var sr = new StreamReader(stream);
                
                columns = sr.ReadLine()?.Split(";");
                rows = sr.ReadToEnd().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);  
            }


            FieldInfo[] fieldInfo = t.GetType().GetFields(BindingFlags.NonPublic
              | BindingFlags.Public
              | BindingFlags.Instance);

            for (int row = 0; row < rows.Length; row++)
            {
                var line = rows[row];

                var parts = line.Split(";");

                for (int i = 0; i < parts.Length; i++)
                {
                    var value = parts[i];
                    var column = columns[i];

                    if (value.IndexOf("\"", StringComparison.Ordinal) == 0)
                        value = value.Substring(1);

                    if (value[^1].ToString() == "\"")
                        value = value[0..^1];

                    if (column.IndexOf("\"") == 0)
                        column = column.Substring(1);

                    if (column[^1].ToString() == "\"")
                        column = column[0..^1];

                    if (column == fieldInfo[i].Name)
                        if (fieldInfo[i].FieldType == typeof(Int32))
                        {
                            fieldInfo[i].SetValue(t, Convert.ToInt32(value)); 

                        }
                }
            }

            //convert from string
            return t;
        }

        public static void Serialize(Stream stream, string value)
        {
            using var sw = new StreamWriter(stream);
                sw.Write(value.ToString().Trim());
        }

       



    }
}
