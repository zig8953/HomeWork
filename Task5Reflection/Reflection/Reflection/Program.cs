﻿using System;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;


namespace Reflection
{
    class Program
    {

        static void Main()
        {
            string json = default;

            #region Serialize
            Stopwatch stopwatch = new Stopwatch();

            //After calling the function
            Serializator.FlagMethod = true;
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                var function = new Data().ToCsv();
                using var stream = new FileStream("Data.csv", FileMode.Create, FileAccess.Write);
                    Serializator.Serialize(stream, function);
            }
            stopwatch.Stop();
            Console.WriteLine($"После вызова функции: {stopwatch.ElapsedMilliseconds};");

            stopwatch.Reset();


            // Without calling the function 
            stopwatch.Start();
            Serializator.FlagMethod = false;

            for (int i = 0; i < 100; i++)
            {
                var Nofunction = new Data().ToCsv();

                using var stream = new FileStream("Data.csv", FileMode.Create, FileAccess.Write);
                    Serializator.Serialize(stream, Nofunction);
            }
            stopwatch.Stop();
            Console.WriteLine($"Без вызова функции: {stopwatch.ElapsedMilliseconds};");
            stopwatch.Reset();

            // JSON serialization
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                json = JsonConvert.SerializeObject(new Data());
            }
            stopwatch.Stop();
            Console.WriteLine($"Cериализация JSON: {stopwatch.ElapsedMilliseconds};");
            stopwatch.Reset();

            stopwatch.Reset();

            #endregion Serialize

            #region Deserialization
            //Serialization
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
                 Serializator.FromCsv<Data>("Data.csv");
        
            stopwatch.Stop();
            Console.WriteLine($"Десериализация: {stopwatch.ElapsedMilliseconds};");

            //JSON deserialization
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                JsonConvert.DeserializeObject<Data>(json);
            }
            stopwatch.Stop();
            Console.WriteLine($"Десериализация JSON: {stopwatch.ElapsedMilliseconds};");
            stopwatch.Reset();

            #endregion Deserialization

            Console.ReadKey();

        }
    }


   
}
