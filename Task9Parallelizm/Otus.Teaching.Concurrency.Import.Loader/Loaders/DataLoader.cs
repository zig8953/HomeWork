using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Data;
using Otus.Teaching.Concurrency.Import.Loader.Lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader
        : IDataLoader, ICustomerRepository
    {
        private Int64 _time = 0;
        
        public void LoadData(string path)
        {
            List<CustomersList> customersLists = new List<CustomersList>();

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Customers",
                IsNullable = true
            };
            using StreamReader reader = new StreamReader(path);
            CustomersList result =
                    (CustomersList)(new XmlSerializer(typeof(CustomersList), xRoot)).Deserialize(reader);

            var splitresult = Split<Customer>(result.Customers, Config.ThreadCount);

            List<Thread> threads = new List<Thread>();
            foreach (var item in splitresult)
            {
                threads.Add(new Thread(() => AddCustomer(item)));

            }

            foreach (var item in threads)
            {
                item.Start();
                item.Join();
            }
        }

        public  void AddCustomer(IEnumerable<Customer> customer)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            _time = 0;

            foreach (var item in customer)
            {
                AddCustomer(item, Config.RetryCount);     
            }

            sw.Stop();
            _time += sw.Elapsed.Seconds;

            Thread thread = Thread.CurrentThread;
            Console.WriteLine($"{thread.ManagedThreadId} Thread running: {sw.ElapsedMilliseconds} ms Total time: {_time} s");
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> list, int parts)
        {
            int i = 0;
            var splits = from item in list
                         group item by i++ % parts into part
                         select part.AsEnumerable();
            return splits;
        }

        public void AddCustomer(Customer customer, int retryCount = 0)
        {
            try
            {
                using (var dataContext = new DBContext())
                {
                        dataContext.Customers.Add(customer);
                        dataContext.SaveChanges();
                };

            }
            catch (Exception)
            {
                if (retryCount > Config.RetryCount)
                    return;

                AddCustomer(customer, retryCount += 1);
            }
        }
    }
}