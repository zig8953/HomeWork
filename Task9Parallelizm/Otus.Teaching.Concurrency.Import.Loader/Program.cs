﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Data;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Configuration;
using Otus.Teaching.Concurrency.Import.Loader.Lib;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static readonly string _processDataFilePath = Config.ProcessDataFilePath;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

        static void Main(string[] args)
        {
            Config.Load();

            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var loader = new DataLoader();
            if (Config.IsProcess)
            {
                ProcessGenerateCustomersDataFile();
                loader.LoadData(_processDataFilePath);
            }
            else
            {
                GenerateCustomersDataFile();
                loader.LoadData(_dataFilePath);
            }
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 10000);
            xmlGenerator.Generate();
        }

        static void ProcessGenerateCustomersDataFile()
        {
            new Processes().ProcessQueue();
        }

    }
}