﻿using Otus.Teaching.Concurrency.Import.Loader.Lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader
{

    class Processes
    {
        private readonly string HandlerProcessFileName = Config.HandlerProcessFileName;
        private readonly string HandlerProcessDirectory = Config.HandlerProcessDirectory; 
        readonly List<Process> _queueHandlerProcesses = new List<Process>();

        public void ProcessQueue()
        {
            var stopWatch = new Stopwatch();

            Console.WriteLine("Process scheduler...");
            Console.WriteLine("Handling queue...");
            stopWatch.Start();

            _queueHandlerProcesses.Add(StartHandlerProcess());

            _queueHandlerProcesses.ForEach(x => x.WaitForExit());

            stopWatch.Stop();

            Console.WriteLine($"Handled queue in {stopWatch.Elapsed}...");
        }

        private Process StartHandlerProcess()
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { "customers" },
                FileName = GetPathToHandlerProcess(),
            };

            var process = Process.Start(startInfo);

            return process;
        }

        private string GetPathToHandlerProcess()
        {
            return Path.Combine(HandlerProcessDirectory, HandlerProcessFileName);
        }

    }
}
