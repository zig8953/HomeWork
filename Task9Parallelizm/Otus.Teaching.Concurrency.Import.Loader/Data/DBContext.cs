﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Lib;

namespace Otus.Teaching.Concurrency.Import.Loader.Data
{
    class DBContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DBContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Config.StrConnection); 
        }
    }
}
