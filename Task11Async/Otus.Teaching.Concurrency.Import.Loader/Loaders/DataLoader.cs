using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Data;
using Otus.Teaching.Concurrency.Import.Loader.Lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader
        : IDataLoader, ICustomerRepository
    {
        private Int64 _time = 0;

        public async void LoadDataAsync(string path)
        {
            List<CustomersList> customersLists = new List<CustomersList>();

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Customers",
                IsNullable = true
            };

  
            using StreamReader reader = new StreamReader(path);
            CustomersList result =
                    (CustomersList)(new XmlSerializer(typeof(CustomersList), xRoot)).Deserialize(reader);

            var splitresult = Split<Customer>(result.Customers, Config.ThreadCount);

            foreach (var item in splitresult)
            {
                await AddCustomerAsync(item);
            } 
        }

        public async Task AddCustomerAsync(IEnumerable<Customer> customer)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            _time = 0;

            foreach (var item in customer)
            {
                await AddCustomerAsync(item, Config.RetryCount);
            }
              
            sw.Stop();
            _time += sw.Elapsed.Seconds;

            Thread thread = Thread.CurrentThread;
            Console.WriteLine($"{thread.ManagedThreadId} Thread running: {sw.ElapsedMilliseconds} ms Total time: {_time} s");
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> list, int parts)
        {
            int i = 0;
            var splits = from item in list
                         group item by i++ % parts into part
                         select part.AsEnumerable();
            return splits;
        }

        public async Task AddCustomerAsync(Customer customer, int retryCount = 0)
        {
            try
            {
                using (var dataContext = new DBContext())
                {
                    await dataContext.Customers.AddAsync(customer);
                    await dataContext.SaveChangesAsync();
                };

            }
            catch (Exception)
            {
                if (retryCount > Config.RetryCount)
                    return;

                _ = AddCustomerAsync(customer, retryCount += 1);
            }
        }
    }
}