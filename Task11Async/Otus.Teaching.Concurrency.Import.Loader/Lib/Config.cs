﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Lib
{
    public static class Config
    {
        public static bool IsProcess { get; set; }
        public static int RetryCount { get; set; }
        public static int ThreadCount { get; set; }
        public static string StrConnection { get; set; }
        public static string HandlerProcessFileName { get; set; }
        public static string HandlerProcessDirectory { get; set; }
        public static string ProcessDataFilePath { get; set; }
        public static void Load()
        {
            var path = @$"{Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location)}\conf.json";

            if (!(File.Exists(path)))
                return;

            var config = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location))
                .AddJsonFile("conf.json").Build();

            IsProcess = Convert.ToBoolean(config.GetSection("IsProcess").Value);
            RetryCount = Convert.ToInt32(config.GetSection("retryCount").Value);
            ThreadCount = Convert.ToInt32(config.GetSection("threadCount").Value);
            StrConnection = config.GetSection("strConnection").Value;
            HandlerProcessFileName = config.GetSection("handlerProcessFileName").Value;
            HandlerProcessDirectory = config.GetSection("handlerProcessDirectory").Value;
            ProcessDataFilePath = config.GetSection("processDataFilePath").Value;

        }
    }
}
