﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelTest
{
    class Program
    {
        private static int[] array = new int[1_000_000] ;
        static Random random = new Random();

        static void Main()
        {
            FillArray();
            TimeTest(() => SumArray());
            TimeTest(() => SumParallelArray());
            TimeTest(() => SumLinqArray());
            Console.ReadKey();
        }

        static void FillArray()
        { 
            array = array.Select((e) => random.Next(0, 100)).ToArray();
        }

        static int SumArray()
        {
            Console.WriteLine("Обычный");
            return array.Sum();
        }


        static int SumParallelArray()
        {
            Console.WriteLine("Параллельное");
            int sum = 0;
            var threads = new List<Thread>();
    
            var splitArray = Split<int>(array, Environment.ProcessorCount);

            foreach (var item in splitArray)
            {
                threads.Add(new Thread(() =>
                {
                    var subsum = item.Sum();
                    Interlocked.Add(ref sum, subsum);
                }));
            }

            threads.ForEach(x => x.Start());
            threads.ForEach(x => x.Join());
            return sum;           
        }

        static int SumLinqArray()
        {
            Console.WriteLine("LINQ");
            return array.AsParallel().Sum();
        }

        static void TimeTest(Func<int> func)
        {
            Stopwatch stopwath = new Stopwatch();
            stopwath.Start();
            Console.WriteLine($"Сумма элементов массива: {func.Invoke()}");
            stopwath.Stop();
            TimeSpan ts = stopwath.Elapsed;
            Console.WriteLine($"Затрачено: {ts.TotalSeconds} секунд");
            Console.WriteLine();
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> list, int parts)
        {
            int i = 0;
            var splits = from item in list
                         group item by i++ % parts into part
                         select part.AsEnumerable();
            return splits;
        }

    }
}
