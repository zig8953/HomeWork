﻿using Microsoft.EntityFrameworkCore;
using Models;

namespace Task17RESTApi.Data
{
    public class SQLLiteContext : DbContext
    {
        public SQLLiteContext(DbContextOptions<SQLLiteContext> options) : base(options)
        {
            Database.EnsureCreated();
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData
              (
                  new User[]
                  {
                       new User() { FullName = "Ivan Ivanov" , Id = 1 },
                       new User() { FullName = "Text Text" , Id = 2}
                  }
              );
        }

        public DbSet<User> users { get; set; }
    }

    

}
