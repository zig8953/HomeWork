﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Task17RESTApi.Service;

namespace Task17RESTApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]


    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
           _userService = userService;
        }

        [HttpGet("GetUser")]
        public IActionResult Get(int Id)
        {
            User user = _userService.Get(Id);

            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (_userService.Create(user))
                return Ok();
            else
                return Conflict();
        }


    }
}
