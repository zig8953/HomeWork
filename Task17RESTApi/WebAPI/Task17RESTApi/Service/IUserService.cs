﻿using Models;

namespace Task17RESTApi.Service
{
    interface IUserService
    {
        User Get(int id);
        bool Create(User value);
    }
}
