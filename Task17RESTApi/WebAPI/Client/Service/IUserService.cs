﻿using Models;
using System.Threading.Tasks;

namespace Client.Service
{
    interface IUserService
    {
        Task<User> Get(int? Id);
        Task<string> Create(User user);
    }
}
