﻿using Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Service
{
    public class UserService : IUserService 
    {
        public HttpClient HttpClient { get; }

        public UserService(HttpClient httpClient)
        {
            if (httpClient.BaseAddress is null)
            {
                HttpClient = httpClient;
            }
        }

        public async Task<User> Get(int? Id)
        { 
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"{HttpClient.BaseAddress}api/User/GetUser?Id={Id}");
            var response = await HttpClient.SendAsync(requestMessage);
           
            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<User>(responseBody));
            }
            else
                return null;
        }

        public async Task<string> Create(User user)
        {
            if (user == null)
                return "CONFLICT";

            string serializedUser = JsonConvert.SerializeObject(user);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "api/User")
            {
                Content = new StringContent(serializedUser)
            };

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await HttpClient.SendAsync(requestMessage);

            return response.StatusCode.ToString();

        }
    }
}