﻿using System;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace SearchURL
{
    class Html
    {
        string pattern = @"[-a-zA-Z0-9@:%_\+.~#?&\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/=]*)?";

        public string Get(string url) 
        {
            if(string.IsNullOrWhiteSpace(url))
                throw new ArgumentException("url не может быть пустым.");

            WebRequest req = HttpWebRequest.Create(url);
            req.Method = "GET";

            using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            }
        }

        public void GerUrl(string html)
        {
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);

            MatchCollection matches = regex.Matches(html);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }
        }
    }
}
