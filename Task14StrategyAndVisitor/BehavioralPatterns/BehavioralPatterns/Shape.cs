﻿
namespace BehavioralPatterns
{
    class Shape 
    {
        public int x;
        public int y;

        public Shape(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public virtual string Export(IVisitor visitor)
            => visitor.Export(this);
        
    }

}
