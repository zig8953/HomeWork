﻿
namespace BehavioralPatterns
{
    interface IVisitor
    {
        string Export(Shape shape);
        string Export(Circle shape);
        string Export(Rectangle shape);
        string Export(Point shape);
    }
}
