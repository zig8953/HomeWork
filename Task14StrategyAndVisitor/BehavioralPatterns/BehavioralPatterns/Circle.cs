﻿
namespace BehavioralPatterns
{
    class Circle : Shape
    {
        public int radius;

        public Circle(int x, int y, int radius)
            : base(x, y)
        {
            this.radius = radius;
        }

        public override string Export(IVisitor visitor)
           => visitor.Export(this);
    }
}