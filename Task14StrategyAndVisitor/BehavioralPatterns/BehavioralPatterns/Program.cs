﻿using System;

namespace BehavioralPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shape = new Shape(10, 10);
            Shape circle = new Circle(30, 50, 10);
            Shape rectangle = new Rectangle(10, 20, 5, 7);
            Shape point = new Point(10, 10);

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("XML");
            IVisitor visitorXML = new VisitorXML();
            Console.WriteLine(circle.Export(visitorXML));
            Console.WriteLine(rectangle.Export(visitorXML));
            Console.WriteLine(point.Export(visitorXML));


            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("JSON");
            IVisitor visitorJSON = new VisitorJSON();
            Console.WriteLine(circle.Export(visitorJSON));
            Console.WriteLine(rectangle.Export(visitorJSON));
            Console.WriteLine(point.Export(visitorJSON));

            Console.ForegroundColor = ConsoleColor.DarkGray;
        }
    }
}
