﻿
namespace BehavioralPatterns
{
    class VisitorXML : IVisitor
    {
        string IVisitor.Export(Shape shape)
        {
            return $@"<Shape> 
                        <x>{shape.x}</x>
                        <y>{shape.y} </y>  
                      </Shape> ";
        }

        string IVisitor.Export(Circle shape)
        {
            return $@"<Circle> 
                        <x>{shape.x}</x>
                        <y>{shape.y} </y>  
                        <r>{shape.radius}</r>
                      </Circle> ";
        }

        string IVisitor.Export(Rectangle shape)
        {
            return $@"<Rectangle> 
                        <x>{shape.x}</x>
                        <y>{shape.y} </y>  
                        <widht>{shape.widht}</widht>  
                        <heignt>{shape.heignt} </heignt>  
                      </Rectangle> ";
        }

        string IVisitor.Export(Point shape)
        {
            return $@"<Point> 
                        <x>{shape.x}</x>
                        <y>{shape.y} </y>  
                      </Point> ";
        }
    }
}