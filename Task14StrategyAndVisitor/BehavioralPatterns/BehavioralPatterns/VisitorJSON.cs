﻿
namespace BehavioralPatterns
{
    class VisitorJSON : IVisitor
    {
        public string Export(Shape shape)
        {
            return $@"{{""x"" : {shape.x},
                        ""y"" : {shape.y}   
                      }} ";
        }

        public string Export(Circle shape)
        {
            return  $@"{{""x"" : {shape.x},
                         ""y"" : {shape.y},   
                         ""R"" : {shape.radius}
                      }} ";
        }

        public string Export(Rectangle shape)
        {
            return $@"{{""x"" : {shape.x},
                        ""y"" : {shape.y},   
                        ""widht"" : {shape.widht}, 
                        ""heignt"" : {shape.heignt},
                      }} ";
        }

        public string Export(Point shape)
        {
            return $@"{{""x"" : {shape.x},
                        ""y"" : {shape.y}   
                      }} ";
        }
    }
}