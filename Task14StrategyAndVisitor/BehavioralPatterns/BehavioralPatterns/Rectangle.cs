﻿
namespace BehavioralPatterns
{
    class Rectangle : Shape
    {
        public int widht, heignt;

        public Rectangle(int x, int y, int widht, int heignt)
            :base(x, y)
        {
            this.widht = widht;
            this.heignt = heignt;
        }

        public override string Export(IVisitor visitor)
            => visitor.Export(this);
    }

}