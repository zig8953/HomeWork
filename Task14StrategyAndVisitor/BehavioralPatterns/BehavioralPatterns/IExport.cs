﻿
namespace BehavioralPatterns
{
    interface IExport
    {
        string Export(Shape shape);
    }
}
