﻿
namespace BehavioralPatterns
{
    class Context
    { 
        private IExport _export;

        public Context(IExport export)
        {
            _export = export;
        }


        public string DoExport(Shape shape)
        {
            return _export.Export(shape);
        }

    }
}