﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using Generate.Lib;
using System.Drawing;

namespace Motivator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Settings.Size = 400;
            Settings.Footer = 50;
            Settings.Indent = 10;
            Settings.DrawBrush = new SolidBrush(System.Drawing.Color.White);
            Settings.DrawFont = new Font("Arial", 16);
        }


    private void BtnGenereteMotivator_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtMessage.Text))
            {
                MessageBox.Show("is null or empty");
                return;
            }
                
            Background background = new Background();
            ImageMotivator imageMotivator = new ImageMotivator();
            Message message = new Message();

            MotivatorFacade motivatorFacade = new MotivatorFacade(background, imageMotivator, message);
            Generate.Lib.Generate generate = new Generate.Lib.Generate();

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*",
                RestoreDirectory = true
            };

            bool? result = dlg.ShowDialog();
            if (result == true)
            { 
                imgMotivator.Source = generate.CreateMotivator(motivatorFacade, txtMessage.Text, dlg.FileName);
            }

        }
    }
}
