﻿using System.Drawing;


namespace Generate.Lib
{
    public class MotivatorFacade
    {
        private readonly Background _background;
        private readonly ImageMotivator _imageMotivator;
        private readonly Message _message;


        public MotivatorFacade(Background background, ImageMotivator imageMotivator, Message message)
        {
            _background = background;
            _imageMotivator = imageMotivator;
            _message = message;
        }

        public Image Start(string message, string path)
        {
            StringFormat stringFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            Bitmap background = _background.Create();
            Image img = _imageMotivator.Load(path);
            RectangleF text = _message.Create();

            using Graphics graphics = Graphics.FromImage(background);
            graphics.Clear(Color.Black);
            graphics.DrawImage(img,
                (Settings.Size - (Settings.Size - Settings.Indent)) / 2,
                 Settings.Indent / 2,
                 Settings.Size - Settings.Indent,
                 Settings.Size - Settings.Indent);

            graphics.DrawString(message, Settings.DrawFont, Settings.DrawBrush, text, stringFormat);

            return background;
        }
    }
}