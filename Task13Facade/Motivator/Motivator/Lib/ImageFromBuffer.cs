﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace Generate.Lib
{
    public class Convector
    {
        public BitmapImage ImageFromBuffer(Image img)
        {
            MemoryStream stream = new MemoryStream(ImageToByteArray(img));
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

        public byte[] ImageToByteArray(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }
}