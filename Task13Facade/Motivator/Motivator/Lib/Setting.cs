﻿using System.Drawing;

namespace Generate.Lib
{
    static class Settings
    {
       public static int Indent { get; set; }
       public static int Size  { get; set; }
       public static int Footer { get; set; }
       public static SolidBrush DrawBrush { get; set; }
       public static Font DrawFont { get; set; }

    }

   
}