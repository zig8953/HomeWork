﻿using System.Drawing;
using System.Windows.Media.Imaging;

namespace Generate.Lib
{
    class Generate
    {

        public BitmapImage CreateMotivator(MotivatorFacade motivatorFacade, string message, string path)
        {
            Convector convector = new Convector();

            Image motivator = motivatorFacade.Start(message, path);

            return convector.ImageFromBuffer(motivator);
        }
    }
}