﻿using System.Drawing;

namespace Generate.Lib
{
    public class ImageMotivator
    {
        public Image Load(string path)
        {
             return Bitmap.FromFile(path);
        }
    }

}
