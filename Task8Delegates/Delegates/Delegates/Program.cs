﻿using System;



namespace Delegates
{
    class Program
    {

        static void Main()
        {
            Console.WriteLine(Task1.Run());

            Console.WriteLine(new string('_', 50));
            Console.WriteLine();

            Task2.Run();

            Console.ReadKey();
        }

    }
}
