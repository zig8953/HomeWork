﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates
{
    static class Task1
    {
        public static string  Run()
        {
            List<Type> linqHelpers = new List<Type>
            {
                new Type { Number = 201 },
                new Type { Number = 15 },
                new Type { Number = 10 },
                new Type { Number = 101 },
                new Type { Number = 500 }
            };

            return $"Max value in type: {Task1.GetMax(linqHelpers, Task1.GetNumber).Number}";
        }

        public static T GetMax<T>(IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            return  e.Where(x => getParametr(x) == e.Select(x => getParametr(x)).Max()).FirstOrDefault();
        }

        public static float GetNumber(Type list)
        {
            if (list == null)
                return 0;

            return list.Number;
        }
    }

    public class Type
    {
        public int Number { get; set; }
    }
}

