﻿using System;
using System.IO;

namespace Delegates
{
    static class Task2
    {
        public static void Run()
        {
            var fileLister = new FileSearcher();
            int filesFound = 0;

            void onFileFound(object sender, FileFoundArgs eventArgs)
            {
                Console.WriteLine(eventArgs.File);
                filesFound++;
            }

            fileLister.FileFound += onFileFound;

            fileLister.DirectoryChanged += (sender, eventArgs) =>
            {
                Console.Write($"Entering '{eventArgs.CurrentSearchDirectory}'.");
                Console.WriteLine($" {eventArgs.CompletedDirs} of {eventArgs.TotalDirs} completed...");
            };

            fileLister.Search(".", "*.json", true);

            fileLister.FileFound -= onFileFound;
        }
    }

    public class FileFoundArgs : EventArgs
    {
        public string File { get; }
        public bool CancelRequested { get; set; }

        public FileFoundArgs(string fileName)
        {
            File = fileName;
        }
    }

    internal class SearchDirectoryArgs : EventArgs
    {
        internal string CurrentSearchDirectory { get; }
        internal int TotalDirs { get; }
        internal int CompletedDirs { get; }

        internal SearchDirectoryArgs(string dir, int totalDirs, int completedDirs)
        {
            CurrentSearchDirectory = dir;
            TotalDirs = totalDirs;
            CompletedDirs = completedDirs;
        }
    }

    public class FileSearcher
    {
        public event EventHandler<FileFoundArgs> FileFound;

        internal event EventHandler<SearchDirectoryArgs> DirectoryChanged
        {
            add { directoryChanged += value; }
            remove { directoryChanged -= value; }
        }
        private EventHandler<SearchDirectoryArgs> directoryChanged;
  
        public void Search(string directory, string searchPattern, bool searchSubDirs = false)
        {
            if (searchSubDirs)
            {
                var allDirectories = Directory.GetDirectories(directory, "*.*", SearchOption.AllDirectories);
                var completedDirs = 0;
                var totalDirs = allDirectories.Length + 1;
                foreach (var dir in allDirectories)
                {
                    directoryChanged?.Invoke(this,
                        new SearchDirectoryArgs(dir, totalDirs, completedDirs++));
                    SearchDirectory(dir, searchPattern);
                }
                directoryChanged?.Invoke(this,
                    new SearchDirectoryArgs(directory, totalDirs, completedDirs++));
                SearchDirectory(directory, searchPattern); 
            }
            else
            {
                SearchDirectory(directory, searchPattern);
            }
        }

        private void SearchDirectory(string directory, string searchPattern)
        {
            foreach (var file in Directory.EnumerateFiles(directory, searchPattern))
            {
                var args = new FileFoundArgs(file);
                FileFound?.Invoke(this, args);
                if (args.CancelRequested)
                    break;
            }
        }
    }
}


