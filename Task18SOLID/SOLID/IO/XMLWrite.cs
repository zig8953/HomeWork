﻿using System;
using System.IO;
using System.Xml;

namespace SOLID.IO
{
    class XMLWrite: IWrite
    {
        private string _fileName;
        private IData _data;

        public XMLWrite(string fileName, IData data)
        {
            _fileName = fileName;
            _data = data;
        }

        public void Write(string text)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement element = xmlDoc.CreateElement(string.Empty, text, string.Empty);
            xmlDoc.AppendChild(element);

            xmlDoc.Save($"{_fileName}.xml");
        }
    }
}