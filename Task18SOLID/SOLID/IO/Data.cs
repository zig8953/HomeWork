﻿using System;

namespace SOLID.IO
{ 
    class Data : IData
    {
        public void Write(string text)
          => Console.Write(text);

        public string Read()
            => (Console.ReadLine());

    }
}