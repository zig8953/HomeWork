﻿using SOLID.IO;
using System;
using System.IO;

namespace SOLID.IO
{
    class TXTWrite: IWrite
    {
        private string _fileName;

        public TXTWrite(string fileName)
        {
            _fileName = fileName; 
        }

        public void Write(string text)
        {
            using (StreamWriter sw = new StreamWriter($"{_fileName}.txt", false, System.Text.Encoding.Default))
            {
                sw.WriteLine(text);
            }
        }
    }
}