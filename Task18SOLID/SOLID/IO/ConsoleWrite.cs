﻿using SOLID.IO;
using System;

namespace SOLID.IO
{
    class ConsoleWrite: IWrite
    {
        public void Write(string text)
        {
            Console.WriteLine(text);
        }
    }
}