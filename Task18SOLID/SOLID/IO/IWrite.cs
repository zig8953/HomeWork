﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.IO
{
    interface IWrite
    {
        void Write(string text);
    }
}
