﻿using System;

namespace SOLID.IO
{
    class DisplayFooter : IFooter
    {
        public void Footer()
        {
            Console.WriteLine("________________");
        }

    }
}
