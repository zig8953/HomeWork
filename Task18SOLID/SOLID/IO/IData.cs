﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.IO
{
    interface IData: IWrite, IRead
    {
        public string Input(string text)
        {
            Write(text);
            return Read();
        }
    }
}
