﻿using SOLID.IO;
using System;

namespace SOLID
{
    class Report
    {
        private string _text { get; set; }
        private IData _data;
        private IWrite _write;
        private IFooter _footer;

        public Report(IData data, IWrite write, IFooter footer)
        {
            _data = data;
            _write = write;
            _footer = footer;
        }

        public void Make()
        {
            Init();
            Header();
            Print();
            _footer.Footer();
        }

        public void Init()
        {
            _text = _data.Input("Введите текст отчета ");
        }

        public void Print()
        {
            _write.Write(_text);    
        }

        public void Header()
        {
            Console.WriteLine("______ШАПКА_____");
        }
       
    }
}