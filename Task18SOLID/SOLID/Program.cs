﻿using SOLID.IO;
using System;

namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            IData data = new Data();
            
            IWrite write = new ConsoleWrite();
            IWrite write1 = new TXTWrite("test");
            IWrite write2 = new XMLWrite("test", data);

            IFooter footer = new DisplayFooter();
            IFooter footer1 = new DisplaySign();

            Report report = new Report(data, write, footer1);
            report.Make();
        }
    }
}
