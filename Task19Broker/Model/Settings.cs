﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using System;

namespace Model
{
    public class Settings
    {        
        public string UserName { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; }
        public string HostName { get; set; }
        public string Key { get; set; }
        public int TimeOut { get; set; }
      
        public Settings()
        {
            IConfiguration Configuration = new ConfigurationBuilder()
            .AddJsonFile("settings.json")
            .AddEnvironmentVariables()
            .Build();

            UserName = Configuration.GetSection("User").Value;
            Password = Configuration.GetSection("Password").Value;
            VirtualHost = Configuration.GetSection("VirtualHost").Value;
            HostName = Configuration.GetSection("HostName").Value;
            Key = Configuration.GetSection("Key").Value;
            TimeOut = Convert.ToInt32(Configuration.GetSection("TimeOut").Value);
        }
    }
}
