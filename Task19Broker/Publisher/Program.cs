﻿using System;
using System.Collections.Generic;
using Model;
using RabbitMQ.Client;

namespace Publisher
{
    class Program
    {
        static void Main()
        {
            Settings settings = new Model.Settings();

            ConnectionFactory factory = new ConnectionFactory()
            {
                UserName = settings.UserName, 
                Password = settings.Password,
                VirtualHost = settings.VirtualHost,
                HostName = settings.HostName
            };

            List<User> users = new List<User>()
            {
                new User{Name = "Ivan", Age = 15,  Email = "Mail.ru" },
                new User{Name = "Ivan", Age = 15,  Email = "Mail.ru" },
                new User{Name = "Ivan", Age = 15,  Email = "Mail.ru" },
                new User{Name = "Ivan", Age = 15,  Email = "Mail.ru" },
                new User{Name = "Ivan", Age = 15,  Email = "" },
                new User{Name = "Jon", Age = 15,  Email = "Mail.ru" },
            };  

            Send send = new Send(factory, users, settings.Key, settings.TimeOut);

            Console.WriteLine("Отправка данных...");
            send.Run();
            Console.WriteLine("Загрузка завешена");

        }   
    }
}

