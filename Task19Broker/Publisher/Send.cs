﻿using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text.Json;
using Model;
using System.Threading;

namespace Publisher
{
    class Send
    {
        private readonly ConnectionFactory _factory;
        private readonly List<User> _users;
        private readonly string _key;
        private readonly int _timeOut;

        public Send(ConnectionFactory factory, List<User> users, string key, int timeOut)
        {
            _factory = factory;
            _users = users;
            _key = key;
            _timeOut = timeOut;
        }

        public void Run()
        {
            if (_factory == null)
                return;

            if (_users == null)
                return;

            if (string.IsNullOrEmpty(_key))
                return;

            Print print = new Print();

            using var connection = _factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: _key,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            foreach (var item in _users)
            {
                string message = JsonSerializer.Serialize(item);
                var body = Encoding.UTF8.GetBytes(message);
                Thread.Sleep(1000);

                channel.BasicPublish(exchange: "",
                                 routingKey: _key,
                                 basicProperties: null,
                                 body: body);
                print.Write(message);
            }
        }
    }
}
