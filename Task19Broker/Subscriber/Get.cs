﻿using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Model;
using System.Threading;
using System.Text.Json;
using System;

namespace Subscriber
{
    class Get
    {
        private readonly ConnectionFactory _factory;
        private readonly string _key;
        private readonly int _timeOut;

        public Get(ConnectionFactory factory, string key, int timeOut)
        {
            _factory = factory;
            _key = key;
            _timeOut = timeOut;
        }

        public void Run()
        {
            if (_factory == null)
                return;

            if (string.IsNullOrEmpty(_key))
                return;

            Print print = new Print();
            User user;

            using var connection = _factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: _key,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (sender, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);


                int dots = message.Split('.').Length - 1;
                Thread.Sleep(dots * _timeOut);

                user = JsonSerializer.Deserialize<User>(message);

                if (user.Email != default)
                {
                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    print.Write(user);
                }
            };
            channel.BasicConsume(queue: _key,
                                 autoAck: false,
                                 consumer: consumer);
        }
    }
}
