﻿using Model;
using RabbitMQ.Client;


namespace Subscriber
{
    class Program
    {
        static void Main()
        {
             Settings settings = new Model.Settings();

            ConnectionFactory factory = new ConnectionFactory()
            {
                UserName = settings.UserName,
                Password = settings.Password,
                VirtualHost = settings.VirtualHost,
                HostName = settings.HostName
            };

            Get get = new Get(factory, settings.Key, settings.TimeOut);
            get.Run();
        }
    }
}
