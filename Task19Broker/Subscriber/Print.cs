﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace Subscriber
{
    public class Print
    {
        public void Write(User user)
        {
            Console.WriteLine($"Получили: {user.Name} Email: {user.Email}" );
        }
    }
}
